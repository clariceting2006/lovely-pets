import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import { useFonts } from 'expo-font';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';

export default function PageNotFound() {
    let [fontsLoaded] = useFonts({
        'Alata': require('../assets/fonts/alata-regular.ttf'),
      });

  return (
    <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/logo.png')} />
        <Text style={styles.text}>Sorry!!</Text>
        <Text style={styles.text}>It seems this page is under</Text>
        <Text style={styles.text}>maintenance</Text>
        <Button
          onPress={onPressLearnMore}
          title="Log In"
          color="#663700"
          accessibilityLabel="Learn more about this purple button"
        />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFEDE2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
      width: 85,
      height: 85,
      marginTop: -20,
      marginBottom: 20
  },
  text: {
    fontFamily: 'Alata',
    fontSize: 22,
    color: '#663700',
  }
});
