import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';
import { useFonts } from 'expo-font';
import { borderColor, color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';

export default function PageNotFound() {
    let [fontsLoaded] = useFonts({
        'Alata': require('../assets/fonts/alata-regular.ttf'),
      });

  return (
    <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/logo.png')} />
        <Text style={styles.text}>Email</Text>
        <TextInput
          style={styles.input}
          placeholder='Email'
        />
        <Text style={styles.text}>Password</Text>
        <TextInput
          style={styles.input}
          placeholder='Password'
        />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFEDE2',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: -70
  },
  logo: {
      width: 85,
      height: 85,
      marginTop: -54,
      marginBottom: 30
  },
  input: {
    height: 40,
    width: 200,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: '#FFFFFF',
    borderColor: '#FFCA8C'
  },
  text: {
    fontFamily: 'Alata',
    fontSize: 22,
    color: '#663700'
  }
});
